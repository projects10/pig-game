# PigGame
A simple 2 player dice game made with HTML5, CSS3 and JavaScript.

### Video link
https://drive.google.com/open?id=1QNsND0sVN2t_Qiy2yFly4W05jh0B6ANX

### Screenshots
![Image1](/screenshots/s1.png)

![Image2](/screenshots/s2.png)
