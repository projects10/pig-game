/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

// total accumulated score of each player
var scores;
// score of the current round
var roundScore;
// which player (1 or 2) is active
var activePlayer;
// prev dice value
var winningScore;

init();

document.getElementById('score-input').addEventListener('blur', ()=>{
    winningScore = parseInt(document.getElementById('score-input').value);
    document.getElementById('score-input').style.border = 'none';
})

// clicking the ROLL DICE button
document.querySelector('.btn-roll').addEventListener('click', ()=>{

    if(winningScore !== 0){
        var dice1 = Math.floor(Math.random() * 6) + 1;
        var dice2 = Math.floor(Math.random() * 6) + 1;

        var dice1DOM = document.querySelector('.dice1');
        var dice2DOM = document.querySelector('.dice2');

        dice1DOM.style.display = 'block';
        dice2DOM.style.display = 'block';
        dice1DOM.src = 'dice-' + dice1 + '.png';
        dice2DOM.src = 'dice-' + dice2 + '.png';

        if(dice1 == 1 || dice2 == 1 || (dice1==6 && dice2==6)){
            document.getElementById('current-'+activePlayer).textContent = 0;
            nextPlayer();
        }
        else{
            roundScore += dice1 + dice2;
            document.getElementById('current-'+activePlayer).textContent = roundScore;
        }
    }
    else{
        document.getElementById('score-input').style.border = '2px solid red';
    }

})

// clicking the HOLD button
document.querySelector('.btn-hold').addEventListener('click', ()=>{
    if(winningScore !== 0){
        scores[activePlayer] += roundScore;
        document.getElementById('score-'+activePlayer).textContent = scores[activePlayer];

        // if game won
        if(scores[activePlayer] >= winningScore){
            // change player background decorations
            document.querySelector('.player-'+activePlayer+'-panel').classList.remove('active');
            document.querySelector('.player-'+activePlayer+'-panel').classList.add('winner');

            // change player name to WINNER
            document.getElementById('name-'+activePlayer).textContent = 'WINNER!';
            // hide roll dice button and hold button
            document.querySelector('.wrapper').classList.add('hide-buttons');
            // hide dice
            document.querySelector('.dice1').style.display = 'none';
            document.querySelector('.dice2').style.display = 'none';
        }
        else{
            nextPlayer();
        }
    }
    else{
        document.getElementById('score-input').style.border = '2px solid red';
    }
})

// clicking the NEW GAME button
document.querySelector('.btn-new').addEventListener('click', init);

function nextPlayer() {

    roundScore = 0;
    activePlayer = (++activePlayer % 2);

    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';

    document.querySelector('.player-0-panel').classList.toggle('active');
    document.querySelector('.player-1-panel').classList.toggle('active');
}

function init(){
    scores = [0,0];
    roundScore = 0;
    activePlayer = 0;
    winningScore = 0;
    document.getElementById('score-input').value = 0;

    document.getElementById('score-input').style.border = 'none';

    // hide dice image
    document.querySelector('.dice1').style.display = 'none';
    document.querySelector('.dice2').style.display = 'none';

    document.getElementById('name-0').textContent = 'Player 1';
    document.getElementById('name-1').textContent = 'Player 2';

    document.querySelector('.player-0-panel').classList.remove('winner');
    document.querySelector('.player-1-panel').classList.remove('winner');

    // set roll and hold buttons to visible
    document.querySelector('.wrapper').classList.remove('hide-buttons');

    document.getElementById('score-0').textContent = '0';
    document.getElementById('score-1').textContent = '0';
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';

    document.querySelector('.player-0-panel').classList.add('active');
    document.querySelector('.player-1-panel').classList.remove('active');
}


